#include "fileSystemTest.hh"
#include <stdio.h>
#include "fileSystem.hh"

FileSystemTest::FileSystemTest(){}
FileSystemTest::~FileSystemTest(){}

// Helper method to run all tests
void FileSystemTest::RunAllTests()
{
  printf("Running All Tests:\n");
  CreateTest();
  DeleteTest();
  MoveTest();
  WriteToFileTest();
}

// Test case for creating an entity
void FileSystemTest::CreateTest()
{

  printf("--- CreateTest\n");
  FileSystem* fs = new FileSystem();

  fs->Create(FileSystem::DRIVE, "harddisk", "\\");
  fs->Create(FileSystem::FOLDER, "user", "\\harddisk");
  fs->Create(FileSystem::FOLDER, "Jared", "\\harddisk\\user");
  fs->Create(FileSystem::ZIPFILE, "myZip.tar.gz", "\\harddisk\\user\\Jared");

  delete fs;
}

void FileSystemTest::DeleteTest()
{
  printf("--- DeleteTest\n");
}

// Test case for moving an entity
void FileSystemTest::MoveTest()
{
  printf("--- MoveTest\n");
  
  FileSystem* fs = new FileSystem();
 
  fs->Create(FileSystem::DRIVE, "harddisk", "\\");
  fs->Create(FileSystem::FOLDER, "user", "\\harddisk");
  fs->Create(FileSystem::FOLDER, "Jared", "\\harddisk\\user");
  fs->Create(FileSystem::ZIPFILE, "myZip.tar.gz", "\\harddisk\\user\\Jared");
  fs->Create(FileSystem::TEXTFILE, "syslog.txt", "\\harddisk");

  fs->Move("\\harddisk\\syslog.txt", "\\harddisk\\user\\Jared\\syslog.txt");
  delete fs;

}

void FileSystemTest::WriteToFileTest()
{
  printf("--- WriteToFileTest\n");
}


int main()
{
  // set up tests
  FileSystemTest* fsTests = new FileSystemTest();

  // run all tests
  fsTests->RunAllTests();

  // tear down tests
  delete fsTests;
}
