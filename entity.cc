#include "entity.hh"
#include <stdio.h>

Entity::Entity()
{
  m_name = "";
  m_path = "";
  m_size = -1;

  printf("Instantiating Entity\n");
}

Entity::Entity(std::string name)
{
  m_name = name;
  m_path = "";
  m_size = -1;
}

Entity::~Entity(){}

std::string Entity::getName()
{
  return m_name;
}

std::string Entity::getPath()
{
  return m_path;
}

int Entity::getSize()
{
  return m_size;
}
