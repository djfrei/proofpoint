#ifndef __FILE_SYSTEM_H__
#define __FILE_SYSTEM_H__


#include <stdio.h>
#include "entity.hh"
#include "drive.hh"
#include "folder.hh"
#include "textFile.hh"

class FileSystem
{
  public:
    enum EntityType
    {
      DRIVE = 0,
      FOLDER,
      TEXTFILE,
      ZIPFILE
    };


    FileSystem();
    ~FileSystem();

    // Creates a new entity.
    STATUS Create(EntityType type, std::string name, std::string parentPath);

    // Deletes an existing entity (and all the entities it contains).
    STATUS Delete(std::string path);

    // Changing the parent of an entity.
    STATUS Move(std::string srcPath, std::string dstPath);

    // Changes the content of a text file.
    STATUS WriteToFile(std::string path, std::string content);

    // This makes the code easier to read
    typedef std::map<std::string, Drive*> DriveMap;

  private:
    DriveMap* m_p_driveMap;

    STATUS navigateToEntity(std::string entityPath, Drive** ent);

  public:
    bool isRoot(std::string path);
    bool isTextFile(Entity* ent);
    bool isDrive(Entity* ent);
    bool isZipFile(Entity* ent);
    bool isFolder(Entity* ent);
    bool hasNextToken(int index, std::string path);
    std::string lastToken(std::string path);
    std::string nextToken(int& index, std::string path);
    std::string pathExceptLastToken(std::string path);
    EntityType getEntityType(Entity* ent);
    STATUS deleteDrive(Drive* drive);
    STATUS deleteFolder(Folder* folder);
    STATUS deleteTextFile(TextFile* txtFile);
    
};
#endif // __FILE_SYSTEM_H__
