#include "drive.hh"
#include "entity.hh"
#include <stdio.h>

Drive::Drive()
{
  m_p_subEntities = new SubEntityMap();
}

Drive::Drive(std::string name) : Entity(name)
{
  Drive();
}

Drive::~Drive()
{
  if (m_p_subEntities)
  {
    SubEntityMap::iterator itr = m_p_subEntities->begin();
    while (itr != m_p_subEntities->end())
    {
      delete itr->second;
    }

    delete m_p_subEntities;
    m_p_subEntities = 0;
  } 
}

STATUS Drive::updateSize()
{
  int size = 0;

  // calculate sum of all subEntity sizes
  SubEntityMap::iterator itr = m_p_subEntities->begin();
  while (itr != m_p_subEntities->end())
  {
    size += itr->second->getSize();
  }

  m_size = size;
  return OK;
}

STATUS Drive::addEntity(Entity* ent)
{
  STATUS retVal = OK;

  std::string entName = ent->getName();

  // check to make sure the item was actually inserted
  std::pair<SubEntityMap::iterator,bool> ret;
  ret = m_p_subEntities->insert(std::make_pair(entName, ent));

  if (ret.second == false)
  {
    printf("ERROR: Could not add entity %s to parent %s\n", entName, m_name);
    retVal = ERROR;
  }
  return retVal;
}

Drive::SubEntityMap* Drive::getSubEntities()
{
  return m_p_subEntities;
}
