#ifndef __ENTITY_H__
#define __ENTITY_H__

#include <string>
#include <map>
#include "status.hh"

class Entity
{
  public:
    Entity();
    Entity(std::string name);
    virtual ~Entity();

    // updateSize() is a purely virtual method implemented differently
    // for each of the following implementations of the Entity abstract
    //  base class:
    //
    // Text file: length of content.
    // Drive    : sum of all sizes of the entities it contains.
    // Folder   : sum of all sizes of the entities it contains.
    // Zip file : 1/2 sum of all sizes of the entities it contains.
    virtual STATUS updateSize() = 0;
    std::string getName();
    std::string getPath();
    int getSize();

  protected:
    std::string m_name;
    std::string m_path;
    int m_size;
};

#endif // __ENTITY_H__
