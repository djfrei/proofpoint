#ifndef __TEXT_FILE_H__
#define __TEXT_FILE_H__

#include "entity.hh"
#include <map>
#include "status.hh"
#include "drive.hh"

class TextFile : public Entity
{
  public:
    TextFile(Drive* parent, std::string name);
    virtual ~TextFile();
    STATUS updateSize();
    void setContent(std::string content);
    std::string getContent();
    Drive* getParent();
    void setParent(Drive* parent);

  protected:
    Drive* m_p_parentEntity;
    std::string m_content;
    void updatePath();

};

#endif // __TEXT_FILE_H__
