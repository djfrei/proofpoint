#include "zipFile.hh"
#include "entity.hh"

ZipFile::ZipFile(Drive* parent, std::string name)
  : Folder(parent, name)
{  
}

STATUS ZipFile::updateSize()
{
  int size = 0;

  // calculate sum of all subEntity sizes
  SubEntityMap::iterator itr = m_p_subEntities->begin();
  while (itr != m_p_subEntities->end())
  {
    size += itr->second->getSize();
  }

  m_size = (size / 2);

  m_p_parentEntity->updateSize();

  return OK;
}
