#ifndef __DRIVE_H__
#define __DRIVE_H__

#include "entity.hh"
#include <map>
#include "status.hh"

class Drive : public Entity
{
  public:
    typedef std::map<std::string, Entity*> SubEntityMap;

    Drive();
    Drive(std::string name);
    virtual ~Drive();
    STATUS updateSize();
    STATUS addEntity(Entity* ent);
    SubEntityMap* getSubEntities();



  protected:
    SubEntityMap* m_p_subEntities;
};

#endif // __DRIVE_H__
