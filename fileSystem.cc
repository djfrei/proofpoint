#include "fileSystem.hh"
#include "entity.hh"
#include "drive.hh"
#include "folder.hh"
#include "zipFile.hh"
#include "textFile.hh"
#include <cstring>

FileSystem::FileSystem()
{
  m_p_driveMap = new DriveMap();
}

FileSystem::~FileSystem()
{
  if (m_p_driveMap)
  {
    DriveMap::iterator itr = m_p_driveMap->begin();
    while (itr != m_p_driveMap->end())
    {
      delete itr->second;
    }

    delete m_p_driveMap;
    m_p_driveMap = 0;
  } 
}

STATUS FileSystem::Create(EntityType type, std::string name, std::string parentPath)
{
  STATUS retVal = OK;
  Drive* parent = NULL;

  if (type == DRIVE)
  {
    if (isRoot(parentPath))
    {
      std::pair<DriveMap::iterator,bool> ret;
      Drive* drive = new Drive(name);
      ret = m_p_driveMap->insert(std::make_pair(name, drive));

      // check to make sure the item was actually inserted
      if (ret.second == false)
      {
        printf("ERROR: Could not add entity %s to parent %s\n", name, parent->getName());
        retVal = ERROR;
      }
    }
    else
    {
      printf("ERROR: Type DRIVE can only be created at the root");
      retVal = ERROR;
    }
  }
  else
  {
    // navigate the filesystem to find the parent
    retVal = navigateToEntity(parentPath, &parent);

    if ((retVal != OK) || (isTextFile(parent)))
    {
      printf("Cannot create the entity because the specified parent path %s is invalid", parentPath);
    }
    else
    {
      Entity* ent = NULL;

      // instantiate a new Entity
      switch (type)
      {
        case FOLDER:
          ent = new Folder(parent, name);

          break;
        case TEXTFILE:
          ent = new TextFile(parent, name);
          break;
        case ZIPFILE:
          ent = new ZipFile(parent, name);
          break;
        default:
          printf("%s::%s, line %d: This should never happen. Entity type = %d\n", __FILE__, __FUNCTION__, __LINE__, type);
          retVal = ERROR;
          break;
      }

      retVal = parent->addEntity(ent);
    }
  }
  return retVal;
}

STATUS FileSystem::Delete(std::string path)
{
  STATUS retVal = OK;
  Drive* ent = NULL;

  retVal = navigateToEntity(path, &ent);

  if (retVal != OK)
  {
    printf("Delete: Unable to navigate entity path");
  }
  else
  {
    EntityType type = getEntityType(ent);

    switch (type)
    {
      case DRIVE:
        retVal = deleteDrive(dynamic_cast<Drive*>(ent));
        break;
      case FOLDER:
      case ZIPFILE:
        retVal = deleteFolder(dynamic_cast<Folder*>(ent)); 
        break; 
      case TEXTFILE:
        retVal = deleteTextFile(dynamic_cast<TextFile*>(ent));
        break;
    }
  }
  return retVal;
}

STATUS FileSystem::Move(std::string srcPath, std::string dstPath)
{
  STATUS retVal = OK;

  Drive* srcEnt = NULL;
  Drive* dstParentEnt = NULL;
  std::string dstParentPath = pathExceptLastToken(dstPath);

  retVal = navigateToEntity(srcPath, &srcEnt);
  retVal = navigateToEntity(dstParentPath, &dstParentEnt);

  if (retVal != OK)
  {
    printf("Error: Cannot move file because cannot navigate srcPath %s or dstPath %s", srcPath, dstPath);
  }
  else
  {
    Drive* dstEnt = dynamic_cast<Drive*>(dstParentEnt);

    Drive::SubEntityMap* subEntMap = dstEnt->getSubEntities();
    std::string name = srcEnt->getName();
    subEntMap->insert(std::make_pair(name, srcEnt));

    EntityType type = getEntityType(srcEnt);

    switch (type)
    {
      case FOLDER:
      case ZIPFILE:
        (dynamic_cast<Folder*>(srcEnt))->setParent(dstEnt);
        break;
      case TEXTFILE:
        (dynamic_cast<TextFile*>(srcEnt))->setParent(dstEnt);
        break;
      default:
        printf("Error: only Folders, Zipfiles, and Textfiles have parents");
        retVal = ERROR;
    }
  }
  return retVal;
}

STATUS FileSystem::WriteToFile(std::string path, std::string content)
{
  return OK;
}

// navigateToEntity assumes that the path given specifies at least
// a drive. For example, you can't pass "\" (filesystem root) as
// into this function and expect appropriate behavior. But you can
// pass in "\c\user\documents".
STATUS FileSystem::navigateToEntity(std::string entityPath, Drive** ent)
{
  STATUS retVal = OK;

  int index = 0;
  std::string myToken = nextToken(index, entityPath);

  // the first token in the path must be a DRIVE
  DriveMap::iterator driveItr = m_p_driveMap->find(myToken);

  if (driveItr == m_p_driveMap->end())
  {
    printf("ERROR: Specified drive %s does not exist!\n", myToken.c_str());
    retVal = ERROR;
  }
  else
  {
    Drive* myEntity = driveItr->second;
    Drive::SubEntityMap* subEntities = myEntity->getSubEntities();


    // continue searching through the file system
    // until you get to the end of the string
    while (!hasNextToken(index, entityPath))
    {
      myToken = nextToken(index, entityPath);
      Drive::SubEntityMap::iterator subEntItr = subEntities->find(myToken);
      
      if (subEntItr == subEntities->end())
      {
        printf("ERROR: Could not find entity %s in the path", myToken);
        retVal = ERROR;
        break;
      }
      else
      {
        myEntity = dynamic_cast<Drive*>(subEntItr->second);

        if (isTextFile(myEntity) && (myToken != lastToken(entityPath)))
        {
          printf("ERROR: Cannot include a text file %s as part of the path", myToken);
        }
        else
        {
          subEntities = (dynamic_cast<Drive*>(myEntity))->getSubEntities();
        }
      }
    }
    
    *ent = myEntity; 
  }
  return retVal;
}

bool FileSystem::isRoot(std::string path)
{
  return ((path.at(0) == '\\') && (path.length() == 1));
}

bool FileSystem::isTextFile(Entity* ent)
{
  TextFile* txtFile = dynamic_cast<TextFile*>(ent);

  if (txtFile)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool FileSystem::isDrive(Entity* ent)
{
  return (!isFolder(ent) && !isTextFile(ent));
}

bool FileSystem::isZipFile(Entity* ent)
{
  ZipFile* zipFile = dynamic_cast<ZipFile*>(ent);

  if (zipFile)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool FileSystem::isFolder(Entity* ent)
{
  Folder* folder = dynamic_cast<Folder*>(ent);

  if (folder && !isZipFile(ent))
  {
    return true;
  }
  else
  {
    return false;
  }
}

std::string FileSystem::nextToken(int& index, std::string path)
{
  int initialIndex = ++index;
  int len = 0;

  while (index < path.length() && path.at(index) != '\\')
  {
    index++;
    len++;
  }

  std::string tokenOut = path.substr(initialIndex, len);

  return tokenOut;
}

bool FileSystem::hasNextToken(int index, std::string path)
{
  return (index <= path.length());
}

std::string FileSystem::lastToken(std::string path)
{
  return "";
}

std::string FileSystem::pathExceptLastToken(std::string path)
{
  int index = path.length()-1;
  int len = 0;
  
  while (path.at(index) != '\\')

  {
    index--;
    len++;
  } 

  return path.substr(index+1, len);
}

FileSystem::EntityType FileSystem::getEntityType(Entity* ent)
{
  EntityType type = DRIVE;

  if (isFolder(ent))
    type = FOLDER;
  else if (isTextFile(ent))
    type = TEXTFILE;
  else if (isZipFile(ent))
    type = ZIPFILE;

  return type;
}

STATUS FileSystem::deleteDrive(Drive* drive)
{
  STATUS retVal = OK;

  std::string driveName = drive->getName();
  DriveMap::iterator driveItr = m_p_driveMap->find(driveName);

  if (driveItr == m_p_driveMap->end())
  {
    printf("Error: Delete: drive %s not in the drive map", driveName); 
    retVal = ERROR;
  }
  else
  {
    // destroy the allocated memory
    delete driveItr->second;

    // erase the map entry
    m_p_driveMap->erase(driveName);
  }

  return retVal;
}

STATUS FileSystem::deleteFolder(Folder* folder)
{
  STATUS retVal = OK;

  Drive::SubEntityMap* subEntMap = folder->getParent()->getSubEntities();

  std::string folderName = folder->getName();
  delete folder;
  subEntMap->erase(folderName);

  return retVal;
}

STATUS FileSystem::deleteTextFile(TextFile* txtFile)
{
  STATUS retVal = OK;

  Drive::SubEntityMap* subEntMap = txtFile->getParent()->getSubEntities();
  
  std::string txtFileName = txtFile->getName();
  delete txtFile;
  subEntMap->erase(txtFileName);

  return retVal;
}
