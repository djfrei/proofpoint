#ifndef __FILE_SYSTEM_TEST_H__
#define __FILE_SYSTEM_TEST_H__

#include "fileSystem.hh"

class FileSystemTest
{
  public:
    FileSystemTest();
    ~FileSystemTest();
    void RunAllTests();

  private:
    void CreateTest();
    void DeleteTest();
    void MoveTest();
    void WriteToFileTest();
};

#endif // __FILE_SYSTEM_TEST_H__
