#include "textFile.hh"
#include "entity.hh"
#include <sstream>

TextFile::TextFile(Drive* parent, std::string name)
  : Entity(name)
{
  m_p_parentEntity = parent;
}

TextFile::~TextFile()
{
}

void TextFile::setContent(std::string content)
{
  m_content = content;
}

std::string TextFile::getContent()
{
  return m_content;
}

STATUS TextFile::updateSize()
{
  m_size = m_content.length();
  return OK;
}

Drive* TextFile::getParent()
{
  return m_p_parentEntity;
}

void TextFile::setParent(Drive* parent)
{
  m_p_parentEntity = parent;
  m_p_parentEntity->updateSize();
  updatePath();
}

void TextFile::updatePath()
{
  std::string parentPath = m_p_parentEntity->getPath();
  std::stringstream ss;
  ss << parentPath << "\\" << m_name;
  m_path = ss.str();
}
