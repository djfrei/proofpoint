#ifndef __ZIP_FILE_H__
#define __ZIP_FILE_H__

#include "entity.hh"
#include "folder.hh"
#include <map>
#include "status.hh"

class ZipFile : public Folder
{
  public:
    ZipFile(Drive* parent, std::string name);
    STATUS updateSize();
};

#endif // __ZIP_FILE_H__
