#ifndef __FOLDER_H__
#define __FOLDER_H__

#include "entity.hh"
#include "drive.hh"
#include <map>
#include "status.hh"

class Folder : public Drive
{
  public:
    Folder(Drive* parent, std::string name);
    virtual ~Folder();
    STATUS updateSize();
    Drive* getParent();
    void setParent(Drive* parent);

  protected:
    Drive* m_p_parentEntity;
    void updatePath();
};

#endif // __FOLDER_H__
