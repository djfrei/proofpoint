#include "folder.hh"
#include "entity.hh"
#include "status.hh"
#include <sstream>

Folder::Folder(Drive* parent, std::string name)
  : Drive(name)
{
  m_p_parentEntity = parent;
  m_p_subEntities = new SubEntityMap();
}

Folder::~Folder()
{
  // remove reference to parent entity
  m_p_parentEntity = 0;

}

STATUS Folder::updateSize()
{
  Drive::updateSize();
  return m_p_parentEntity->updateSize();
}

Drive* Folder::getParent()
{
  return m_p_parentEntity;
}

void Folder::setParent(Drive* parent)
{
  m_p_parentEntity = parent;
  m_p_parentEntity->updateSize();
  updatePath();
}

void Folder::updatePath()
{
  std::string parentPath = m_p_parentEntity->getPath();
  std::stringstream ss;
  ss << parentPath << "\\" << m_name;
  m_path = ss.str();
}

