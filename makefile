CC=gcc
CXX=g++ -g
RM=rm -f
CPPFLAGS=-g -Wall 
LDFLAGS=-g
#LDLIBS=$(shell root-config --libs)

SRCS= \
      fileSystemTest.cc \
      fileSystem.cc \
      entity.cc \
      drive.cc \
      folder.cc \
      zipFile.cc \
      textFile.cc

OBJS=$(subst .cc,.o,$(SRCS))

all: fileSystemTest

fileSystemTest: $(OBJS)
	$(CXX) -o fileSystemTest $(OBJS)

fileSystemTest.o: fileSystemTest.hh fileSystemTest.cc

fileSystem.o: fileSystem.hh fileSystem.cc

entity.o: entity.hh entity.cc

drive.o: drive.hh drive.cc

folder.o: folder.hh folder.cc

textFile.o: textFile.hh textFile.cc

zipFile.o: zipFile.hh zipFile.cc

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) fileSystemTest
